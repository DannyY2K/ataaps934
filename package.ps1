$FileName = "package.zip"

if (Test-Path $FileName) {
  Remove-Item $FileName
  write-host "$FileName has been deleted"
}
Compress-Archive "./dist/*" package.zip