# UX features

1. Popup

   - Links open up in new options windows if not previously open
   - reuse tab if available
   - reused tabs opend by popup and open by chrome context menu.
   - switch to options tab when clicking popup menu

2. Labor Window
   - Update Options with no page reload
   - shortcuts for creating new schedules
   - bidirectional options updates
3. Leave Request View
   - update options with no page reload
   - shorcuts for creating new leave requests
   - bidirectional options updates
