# ATAAPS Chrome Extension

### Why is this here

1. ATAAPS lacks some of the features modern web applications have, like autocomplete and search features.
2. While ATAAPS has a feature to automatically fill out your timecard every paid period some organizations either dont do it, or want to do it.
3. but lastly this is made because i can (well I wanted to see if i could.)

### Features

- Save timecard templates
  - schedules can be saved and reused every pay period.
  - Templates can be edited
  - Template are injected into the ATAAPS labor window to auto fill time card
- Create Leave request
  - Create individual or range leave request
  - option for skipping RDO (RDO can be calculated based on a previous RDO date)
  - Leave request are injected into the ATAAPS new leave request page.
- ATAAPS Options automatically pulled out of ATAAPS
  - While I dont have access to ATAAPS data, Chrome extensions give me the ability to pull these options from the page.
  - Options can only be pulled from page elements that are not disabled, they have to be available to be pulled.

### Installation

Looks for [ataaps934](https://chrome.google.com/webstore/search/ataaps934) inside the Chrome Extension Store

# **First time setup.**

- To allow the extension to get the ATAAPS options
  - Goto to the labor page and add a new row (this pull the options for schedule templates)
  - ATAAPS Menu => Labor => insert row
- To get the leave request options
  - Go to the create new leave request window (that's it)
  - ATAAPS Menu => Leave Request => New Leave Request
- Verifying data
  - To verefied that ATAAPS data has been loaded, click on the extension icon and then click on "show data"
