import { LeaveRequest } from '@/helpers/LeaveRequest';
import { Schedule } from '@/helpers/Schedule';
import { ChromeStorageKeys } from '@/helpers/types';
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export interface OptionItem {
  text: string;
  value: string;
  [key: string]: any
}
export interface LaborWindowOptions {
  typeHour: OptionItem[],
  userData: OptionItem[],
  costCenter: OptionItem[]
}

export interface leaveReqWindowOptions {
  typeHour: OptionItem[],
  certifiers: OptionItem[],
}

export interface RootState {
  laborWindow: LaborWindowOptions;
  leaveReqWindow: leaveReqWindowOptions;
  savedSchedules: Schedule[];
  savedLeaveRequests: LeaveRequest[];
  rdoDate: string;
}


export default new Vuex.Store<RootState>({
  state: {
    laborWindow: {
      typeHour: [],
      userData: [],
      costCenter: []
    },
    leaveReqWindow: {
      typeHour: [],
      certifiers: []
    },
    savedSchedules: [],
    savedLeaveRequests: [],
    rdoDate: ""
  },
  getters: {
    chromeStorate(state: RootState) {
      let hasRdo = state.rdoDate && state.rdoDate.trim() != "";
      return {
        laborWindowTypeHourOptions: state.laborWindow.typeHour,
        laborWindowUserDataOptions: state.laborWindow.userData,
        laborWindowCostCenterOptions: state.laborWindow.costCenter,
        savedSchedules: state.savedSchedules,
        leaveRequestWindowTypeHourOptions: state.leaveReqWindow.typeHour,
        leaveRequestWindowCertifierOptions: state.leaveReqWindow.certifiers,
        savedLeaveRequests: state.savedLeaveRequests,
        rdoDate: hasRdo ? [state.rdoDate] : []
      }
    },
    rdoDateFormatted(state: RootState) {
      let result = null;
      if (state.rdoDate && state.rdoDate.trim() != "") {
        let [year, month, day] = state.rdoDate.split("-");
        result = `${month}/${day}/${year}`;
      }
      return result;
    }
  },
  mutations: {
    setRdo(state: RootState, rdo: string) {
      state.rdoDate = rdo;
    },
    setTypeHour(state: RootState, options: OptionItem[]) {
      state.laborWindow.typeHour = options
    },
    setUserData(state: RootState, options: OptionItem[]) {
      state.laborWindow.userData = options
    },
    setCostCenter(state: RootState, options: OptionItem[]) {
      state.laborWindow.costCenter = options
    },
    setSchedules(state: RootState, schedules: Schedule[]) {
      state.savedSchedules = schedules
    },
    addSchedules(state: RootState, schedules: Schedule[]) {
      state.savedSchedules.push(...schedules)
    },
    spliceSchedule(state: RootState, { index, schedule }: { index: number, schedule: Schedule }) {
      if (schedule) {
        state.savedSchedules.splice(index, 1, schedule)
      } else {
        state.savedSchedules.splice(index, 1)
      }
    },
    //Leave request
    setTypeHourLeaveRequest(state: RootState, options: OptionItem[]) {
      state.leaveReqWindow.typeHour = options
    },
    setCertifiers(state: RootState, options: OptionItem[]) {
      state.leaveReqWindow.certifiers = options
    },
    setLeaveRequest(state: RootState, leaveRequests: LeaveRequest[]) {
      state.savedLeaveRequests = leaveRequests
    },
    addLeaveRequests(state: RootState, leaveRequests: LeaveRequest[]) {
      state.savedLeaveRequests.push(...leaveRequests)
    },
    spliceLeaveRequest(state: RootState, { id, leaveRequest }: { id: string, leaveRequest: LeaveRequest }) {
      let index = state.savedLeaveRequests.findIndex(l => l.id == id)
      if (index > -1)
        if (leaveRequest) {
          state.savedLeaveRequests.splice(index, 1, leaveRequest)
        } else {
          state.savedLeaveRequests.splice(index, 1)
        }
    }

  },
  actions: {
    setTypeHourOpts({ commit }, options: OptionItem[]) {
      commit('setTypeHour', options)
    },
    setUserDataOpts({ commit }, options: OptionItem[]) {
      commit('setUserData', options)
    },
    setCostCenterOpts({ commit }, options: OptionItem[]) {
      commit('setCostCenter', options)
    },
    async addSchedules({ commit, state }, schedules: Schedule[]) {
      commit("addSchedules", schedules);
      await chrome.storage.local.set({ [ChromeStorageKeys.SAVED_SCHEDULES]: state.savedSchedules.map(i => i.toJson()) });
    },
    async deleteSchedule({ commit, state }, index: number) {
      commit("spliceSchedule", { index });
      await chrome.storage.local.set({ [ChromeStorageKeys.SAVED_SCHEDULES]: state.savedSchedules.map(i => i.toJson()) });
    },
    setSchedules({ commit }, schedules: Schedule[]) {
      // Initial setter.
      commit("setSchedules", schedules)
    },
    async updateSchedule({ commit, state }, schedule: Schedule) {
      let index = state.savedSchedules.findIndex(item => item.id == schedule.id);
      if (index > -1) {
        commit("spliceSchedule", { index, schedule: new Schedule(schedule.toJson()) })
        await chrome.storage.local.set({ [ChromeStorageKeys.SAVED_SCHEDULES]: state.savedSchedules.map(i => i.toJson()) });
      }
    },
    //leaveRequest
    setTypeHourLeaveRequest({ commit }, options: OptionItem[]) {
      commit("setTypeHourLeaveRequest", options)
    },
    setCertifiers({ commit }, options: OptionItem[]) {
      commit("setCertifiers", options)
    },
    setLeaveRequest({ commit }, leaveRequests: LeaveRequest[]) {
      commit("setLeaveRequest", leaveRequests)
    },
    async addLeaveRequests({ commit, state }, leaveRequests: LeaveRequest[]) {
      commit("addLeaveRequests", leaveRequests);
      await chrome.storage.local.set({ [ChromeStorageKeys.SAVED_LEAVE_REQS]: state.savedLeaveRequests.map((i) => i.toJson()) });
    },
    async deleteLeaveRequest({ commit, state }, id: string) {
      commit("spliceLeaveRequest", { id })
      await chrome.storage.local.set({ [ChromeStorageKeys.SAVED_LEAVE_REQS]: state.savedLeaveRequests.map((i) => i.toJson()) });

    },
    async updateLeaveRequest({ commit, state }, leaveRequest: LeaveRequest) {
      let index = state.savedLeaveRequests.findIndex(item => item.id == leaveRequest.id);
      if (index > -1) {
        commit("spliceLeaveRequest", { id: leaveRequest.id, leaveRequest: new LeaveRequest(leaveRequest.toJson()) })
        await chrome.storage.local.set({ [ChromeStorageKeys.SAVED_LEAVE_REQS]: state.savedLeaveRequests.map((i) => i.toJson()) });
      }
    },
    async setRdo({ commit }, rdo: string) {
      commit("setRdo", rdo)
      await chrome.storage.local.set({ ['rdoDate']: rdo });
    }
  },
  modules: {},
});
