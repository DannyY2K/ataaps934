import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import LeaveRequestView from "@/components/options/LeaveRequestView.vue";
import LaborWindow from "@/components/options/LaborWindow.vue";
import Home from "@/components/options/Home.vue";
import NotFound from "@/components/options/NotFound.vue";
import AtaapsData from "@/components/options/AtaapsData.vue"

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/leave-request',
    name: 'leaveRequest',
    component: LeaveRequestView
  },
  {
    path: '/labor-window',
    name: 'LaborWindow',
    component: LaborWindow
  },
  {
    path: '/ataaps-data',
    name: 'AtaapsData',
    component: AtaapsData
  },
  {
    path: '*',
    name: 'notFound',
    component: NotFound
  }
]

const router = new VueRouter({
  mode: 'hash',// it was history
  base: '/popup.html',
  routes
})

export default router
