import { getElement } from '@/helpers/helperFunctions';
import { LeaveRequest, leaveRequestURLs } from '@/helpers/LeaveRequest';
import { ChromeStorageKeys } from '@/helpers/types';

let elementMap = {
    typeHour: '#typehrs',
    certifiers: '#certifiers',
    fromDate: "#f_date_c2",
    toDate: "#f_date_c3",
    totalHours: "#durationHours",
    totalMins: "#durationMinutes",
    fromHour: "#fromHour",
    fromMinutes: "#fromMinutes",
    toHour: "#toHour",
    toMinutes: "#toMinutes",
}

function onSelect(event: Event) {
    let { selectedOptions, options } = event.target as HTMLSelectElement;
    let selectedOption = JSON.parse(selectedOptions[0]?.value) as LeaveRequest;
    if (selectedOption.fromDate) {
        getElement<HTMLSelectElement>(elementMap.typeHour).value = selectedOption.typeHour.value;
        getElement<HTMLInputElement>(elementMap.fromDate).value = LeaveRequest.toDisplayDate(selectedOption.fromDate)

        getElement<HTMLSelectElement>(elementMap.fromHour).value = `${selectedOption.fromDate.hour}`;
        getElement<HTMLSelectElement>(elementMap.fromMinutes).value = `${selectedOption.fromDate.minute}`;

        getElement<HTMLInputElement>(elementMap.toDate).value = LeaveRequest.toDisplayDate(selectedOption.toDate)
        getElement<HTMLSelectElement>(elementMap.toHour).value = `${selectedOption.toDate.hour}`;
        getElement<HTMLSelectElement>(elementMap.toMinutes).value = `${selectedOption.toDate.minute}`;
        getElement<HTMLInputElement>(elementMap.totalHours).value = `${selectedOption.totalHours}`;
        getElement<HTMLSelectElement>(elementMap.totalMins).value = `${selectedOption.totalMins}`
        getElement<HTMLSelectElement>(elementMap.certifiers).value = selectedOption.certifier.value;

    }
}

function clearPage() {
    getElement<HTMLSelectElement>('#arnoldSelectElement')?.remove();
    getElement<HTMLTableRowElement>('#arnoldNewRow')?.remove();
}

function createSelect(opts: LeaveRequest[]) {
    let selectElement = document.createElement('select');
    selectElement.id = "arnoldSelectElement"

    selectElement.style.backgroundColor = 'orange';
    let emptyElement = document.createElement("option");
    emptyElement.text = '';
    emptyElement.value = JSON.stringify({});
    selectElement.prepend(emptyElement);

    for (var i = 0; i < opts.length; i++) {
        let { month, day, year } = opts[i].fromDate
        let label = `${month + 1}/${day}/${year}`;
        let option = document.createElement("option");
        option.value = JSON.stringify(opts[i]);
        option.text = label;
        selectElement.appendChild(option);
    };
    selectElement.onchange = onSelect;
    return selectElement
}

async function main(savedLeaveRequests?: LeaveRequest[]) {
    clearPage();
    let requestContainer = document.querySelector("#requestForm")?.querySelector('tbody');
    let newRow = document.createElement('tr');
    newRow.id = 'arnoldNewRow'
    let newTd = document.createElement('td')
    let newLabel = document.createElement('td')
    newRow.append(newTd)
    newRow.append(newLabel)
    requestContainer?.prepend(newRow);

    let opts = savedLeaveRequests ? { savedLeaveRequests } : await chrome.storage.local.get([ChromeStorageKeys.SAVED_LEAVE_REQS]);
    let placeHolder = document.createElement("div").innerText = "No Leave Request Found";
    if (opts.savedLeaveRequests && opts.savedLeaveRequests.length > 0) {
        newTd.append(createSelect(opts.savedLeaveRequests));
        newLabel.append("Pick Leave Request")
    } else {
        newTd.append(placeHolder);
    }


}


chrome.storage.onChanged.addListener(function (changes, namespace) {
    let keyChange = changes.savedLeaveRequests;
    if (keyChange) {
        main(keyChange.newValue)
    }
});
let currentUrl = new URL(window.location.href);
let allowedURLs = leaveRequestURLs.map(i => i.toLowerCase());
let isNewReqWin = allowedURLs.indexOf(currentUrl.href.toLowerCase()) > -1;
if (isNewReqWin)
    main();

console.log("Leave request content running")    