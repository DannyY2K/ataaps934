import { Schedule, ScheduleOptions } from "@/helpers/Schedule";
import { ChromeStorageKeys } from "@/helpers/types";
import { nanoid } from 'nanoid'

let elementMap = {
    costCenter: {
        getElement: (index: number): HTMLSelectElement => document.querySelector(`[name='wc${index}']`) as HTMLSelectElement
    },
    userData: {
        getElement: (index: number): HTMLSelectElement => document.querySelector(`[name='userDefCode${index}']`) as HTMLSelectElement
    },
    typeHour: {
        getElement: (index: number): HTMLSelectElement => document.querySelector(`[name='typeHr${index}']`) as HTMLSelectElement
    },
    day: {
        getElement: (day: number, row: number): HTMLInputElement => document.querySelector(`[name='day${day}r${row}']`) as HTMLInputElement
    }
}

function onSelect(event: Event) {
    let { parentId, selectedOptions } = event.target as mySelect;
    let scheduleId = parseInt(parentId.substring(4));
    let scheduleOptions = JSON.parse(selectedOptions[0].value) as ScheduleOptions;

    elementMap.costCenter.getElement(scheduleId).value = scheduleOptions.costCenter?.value || "";
    elementMap.userData.getElement(scheduleId).value = scheduleOptions.userData?.value || "";
    elementMap.typeHour.getElement(scheduleId).value = scheduleOptions.typeHour?.value || "";
    (scheduleOptions.hours || new Array(14).fill('')).forEach((hours, day) => {
        elementMap.day.getElement(day + 1, scheduleId).value = `${hours}`
    })
}

interface mySelect extends HTMLSelectElement {
    parentId: string
}


function createSelect(parentId: string, opts: any[]) {
    let elementName = `arnoldsSelectElement-${nanoid(5)}`;
    //Clone options so that it does not make duplicate opts.
    let options = [{ name: "" }, ...opts]
    let selectElement = document.createElement('select') as mySelect;
    selectElement.style.float = 'left';
    selectElement.style.backgroundColor = 'orange';
    selectElement.style.width = "40%";
    selectElement['parentId'] = parentId;
    selectElement.name = elementName;
    for (var i = 0; i < options.length; i++) {
        var option = document.createElement("option");
        option.value = JSON.stringify(options[i]);
        option.text = options[i].name;
        selectElement.appendChild(option);
    };
    selectElement.onchange = onSelect;
    return selectElement
}


function clearPage() {
    document.querySelectorAll(`select[name^="arnoldsSelectElement"]`).forEach(el => {
        el.remove();
    })

    document.querySelectorAll(`div[id^="arnoldsDivElement"]`).forEach(el => {
        el.remove();
    })
}
async function main(savedSchedules?: Schedule[]) {
    clearPage();
    let continerIdRegex = new RegExp('^flsa[0-9]+$');
    let opts = savedSchedules ? { savedSchedules } : await chrome.storage.local.get([ChromeStorageKeys.SAVED_SCHEDULES]);

    if (opts?.savedSchedules) {
        document.querySelectorAll("td[id^=flsa]").forEach((element) => {
            if (continerIdRegex.test(element.id)) {
                element.prepend(createSelect(element.id, opts.savedSchedules));
            }
        })
    } else {
        document.querySelectorAll("td[id^=flsa]").forEach((element) => {
            if (continerIdRegex.test(element.id)) {
                let div = document.createElement("div");
                div.innerText = "No Schedules Found";
                div.id = `arnoldsDivElement-${nanoid(5)}`;
                div.style.float = 'left';
                div.style.backgroundColor = 'orange';
                div.style.width = "40%";
                element.prepend(div);
            }
        })
    }
}

chrome.storage.onChanged.addListener(function (changes, namespace) {
    let keyChange = changes[ChromeStorageKeys.SAVED_SCHEDULES];
    if (keyChange) {
        main(keyChange.newValue)
    }
});

let currentUrl = new URL(window.location.href);
let allowedURL = "https://ataaps.csd.disa.mil/ataaps_ar4/controllerservlet"
let isLaborWin = currentUrl.searchParams.get("name") == "LaborWindow.jsp" || currentUrl.href.toLowerCase() == allowedURL;
if (isLaborWin) {
    console.log('Running labor content script')
    main()
}