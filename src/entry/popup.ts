async function navigate(to: string) {
    let openTab = await chrome.tabs
        .query({ url: chrome.runtime.getURL(`options.html`) })
        .then((tabs) => tabs[0])
        .catch((e) => null);
    let tabId = "betterAtaaps";
    let url = chrome.runtime.getURL(`options.html${to}`);
    if (openTab?.id) {
        chrome.tabs.update(openTab?.id, { url, active: true });
    } else {
        window.open(url, tabId);
    }
    window.close()
}

document.querySelectorAll("button").forEach(button => {
    switch (button.id) {
        case "home":
            button.addEventListener("click", () => {
                navigate("#/")
            })
            break;
        case "labor":
            button.addEventListener("click", () => {
                navigate("#/labor-window")
            })
            break;
        case "leave":
            button.addEventListener("click", () => {
                navigate("#/leave-request")
            })
            break;
        case "data":
            button.addEventListener("click", () => {
                navigate("#/ataaps-data")
            })
            break;
    }
})