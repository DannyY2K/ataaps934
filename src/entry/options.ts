import Vue from "vue";
import App from "../view/options.vue";
import vuetify from "@/plugins/vuetify"
import router from "@/router/options"
import store from "@/store/optionPageStore"
import { filters } from "@/helpers/filters"
import "@mdi/font/css/materialdesignicons.css"

Vue.config.productionTip = false;
filters.forEach(({ name, func }) => {
  Vue.filter(name, func)
});


new Vue({
  vuetify,
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
