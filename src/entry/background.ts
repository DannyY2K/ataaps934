import { ChromeStorageKeys } from "@/helpers/types";


interface webNavigationEventDetails {
    documentId: string;
    documentLifecycle: string;
    frameId: number;
    frameType: string;
    parentFrameId: number;
    processId: number;
    tabId: number;
    timeStamp: number;
    url: string;
}

const filter = {
    url: [
        {
            urlMatches: 'https://ataaps.csd.disa.mil',
        },
    ],
};

function handleLaborWindow(tabId: number) {
    // WARNING: cannot import functions outside of the scope of the "func" key
    chrome.scripting.executeScript(
        {
            target: { tabId },
            func: () => {
                function getElement<T extends Element>(selector: string): T {
                    return document.querySelector(selector) as T;
                }

                let typeHourElement = getElement<HTMLSelectElement>("select[name='typeHr1']");

                let userDataElement = getElement<HTMLSelectElement>("select[name='userDefCode1']");
                let costCenterElement = getElement<HTMLSelectElement>("select[name='wc1']");

                let typeHourOptions: any[] = [];
                let userDataOptions: any[] = [];
                let costCenterOptions: any[] = [];

                if (typeHourElement) {
                    typeHourOptions = Array.from(typeHourElement.options).map(
                        ({ value, text }) => ({ value, text })
                    );
                }

                if (userDataElement) {
                    userDataOptions = Array.from(userDataElement.options).map(
                        ({ value, text }) => ({ value, text })
                    );
                }

                if (costCenterElement) {
                    costCenterOptions = Array.from(costCenterElement.options).map(
                        ({ value, text }) => ({ value, text })
                    );
                }
                return { typeHourOptions, userDataOptions, costCenterOptions };
            },
        },
        (injectionResults) => {
            if (injectionResults[0].result) {
                let { typeHourOptions, userDataOptions, costCenterOptions } = injectionResults[0].result;
                typeHourOptions.length > 0 && chrome.storage.local.set({ [ChromeStorageKeys.LABOR_TYPE_HOUR_OPTS]: typeHourOptions })
                userDataOptions.length > 0 && chrome.storage.local.set({ [ChromeStorageKeys.LABOR_USER_DATA_OPTS]: userDataOptions })
                costCenterOptions.length > 0 && chrome.storage.local.set({ [ChromeStorageKeys.LABOR_COST_CENTER_OPTS]: costCenterOptions })
            }
        }
    );
}

function handleLeaveRequestWindow(tabId: number) {
    // WARNING: cannot import functions outside of the scope of the "func" key
    chrome.scripting.executeScript(
        {
            target: { tabId },
            func: () => {
                function getElement<T extends Element>(selector: string): T {
                    return document.querySelector(selector) as T;
                }
                let typeHourElement = getElement<HTMLSelectElement>("select[name='leaveRequest.typeHourSysNum']");
                let certifiersElement = getElement<HTMLSelectElement>("select[name='certifiers']");

                let typeHourOptions: any[] = [];
                let certifiersOptions: any[] = [];

                if (typeHourElement) {
                    typeHourOptions = Array.from(typeHourElement.options).map(
                        ({ value, text }) => ({ value, text })
                    );
                }

                if (certifiersElement) {
                    certifiersOptions = Array.from(certifiersElement.options).map(
                        ({ value, text }) => ({ value, text })
                    );
                }


                return { typeHourOptions, certifiersOptions };
            },
        },
        (injectionResults) => {
            let { typeHourOptions, certifiersOptions } = injectionResults[0].result;
            typeHourOptions.length > 0 && chrome.storage.local.set({ [ChromeStorageKeys.LEAVE_TYPE_HOUR_OPTS]: typeHourOptions })
            certifiersOptions.length > 0 && chrome.storage.local.set({ [ChromeStorageKeys.LEAVE_CERTIFIER_OPTS]: certifiersOptions })
        }
    );
}

chrome.webNavigation.onCompleted.addListener(async (details) => {
    let { url, tabId } = details as webNavigationEventDetails;
    let currentUrl = new URL(url);
    let isLaborWin = currentUrl.searchParams.get("name") == "LaborWindow.jsp" || currentUrl.href.toLowerCase() == "https://ataaps.csd.disa.mil/ataaps_ar4/controllerservlet";
    let isLeaveReqWin = currentUrl.pathname.includes("request-input.action")
    if (isLaborWin) {
        handleLaborWindow(tabId)
    }

    if (isLeaveReqWin) {
        handleLeaveRequestWindow(tabId)
    }

}, filter)

