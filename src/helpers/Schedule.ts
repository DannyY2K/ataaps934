import { OptionItem } from "@/store/optionPageStore";
import { nanoid } from 'nanoid'
const handler = {
    get(target: Schedule, prop: any, receiver: any) {
        if (typeof prop == "string" && prop.startsWith("day")) {
            let day = parseInt(prop.split("day")[1]) - 1
            return target.hours[day];
        }
        return Reflect.get(target, prop, receiver);
    },
};

export interface ScheduleOptions {
    hours?: number[];
    typeHour?: OptionItem;
    costCenter?: OptionItem;
    userData?: OptionItem
    name: string,

}

export class Schedule {
    hours: number[] = [0, 9, 9, 9, 9, 8, 0, 0, 9, 9, 9, 9, 0, 0];
    typeHour?: OptionItem;
    costCenter?: OptionItem;
    userData?: OptionItem;
    name: string = "";
    id: string;
    constructor({ hours, typeHour, costCenter, userData, name }: ScheduleOptions) {
        if (hours)
            this.hours = hours
        this.typeHour = typeHour;
        this.costCenter = costCenter;
        this.userData = userData
        this.name = name;
        this.id = nanoid();
        return new Proxy(this, handler)
    }

    toJson() {
        let { hours, typeHour, costCenter, userData, name } = this;
        return { hours, typeHour, costCenter, userData, name }
    }

}

export enum HourTypes {
    RG = "RG",
    LN = "LN",
    LH = "LH",
    OTH = "OTH",
}