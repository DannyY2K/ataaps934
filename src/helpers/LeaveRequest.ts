import { OptionItem } from "@/store/optionPageStore";
import { nanoid } from "nanoid";

export interface DateObject {
    month: number;
    year: number;
    day: number;
    hour: number;
    minute: number
}

export interface LeaveRequestOpts {
    typeHour: OptionItem;
    certifier: OptionItem;
    fromDate: DateObject;
    toDate: DateObject;
    totalHours: number;
    totalMins: number;
}

export class LeaveRequest {
    typeHour: OptionItem = { text: '', value: '' }
    certifier: OptionItem = { text: '', value: '' }
    fromDate: DateObject = { month: 0, day: 0, year: 0, hour: 0, minute: 0 }
    toDate: DateObject = { month: 0, day: 0, year: 0, hour: 0, minute: 0 }
    totalHours: number = 0;
    totalMins: number = 0;
    private _id: string;

    get id() {
        return this._id
    }

    constructor(options: LeaveRequestOpts) {
        Object.assign(this, options);
        this._id = nanoid()
    }

    toJson() {
        let { typeHour,
            certifier,
            fromDate,
            toDate,
            totalHours,
            totalMins } = this;
        return {
            typeHour,
            certifier,
            fromDate,
            toDate,
            totalHours,
            totalMins
        }
    }

    static toDisplayDate(date: DateObject) {
        let { month, day, year } = date;
        return [month + 1, day, year].map(n => `${n}`.padStart(2, '0')).join("/");
    }

    overrideId(newId: string) {
        this._id = newId
    }
}

export const leaveRequestURLs = ["https://ataaps.csd.disa.mil/ataaps_AR4/request/leave/request-input.action", "https://ataaps.csd.disa.mil/ataaps_AR4/request/leave/save-leave-request.action"]