export const filters = [
    {
        name: "dayOfTheWeek",
        func: (day: number) => {
            return ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"][day];
        }
    },
    {
        name: "stringTrim",
        func: (value: any) => {
            return value ? value.replace(/\s\s+/g, " - ") : value;
        },
    },
    {
        // Magic of the internet: https://stackoverflow.com/a/51273680
        name: "deCamelCase",
        func: (str: string) =>
            str.replace(/[A-Z]/g, " $&").replace(/^./, (str) => str.toUpperCase())
    }
]