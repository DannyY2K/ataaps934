export enum AtaapsEvents {
    UPDATE_LABOR_OPTS = "update_labor_windows_options",
    UPDATE_LEAVE_OPTS = "update_leave_request_options"
}

export interface AtaapsMessage {
    type: AtaapsEvents,
    payload?: any
    tabId?: number
}

export const daysOfTheWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

export enum NewLeaveRequestDialogMode {
    EDIT,
    NEW_REQUEST
}

export enum ChromeStorageKeys {
    LABOR_TYPE_HOUR_OPTS = "laborWindowTypeHourOptions",
    LABOR_USER_DATA_OPTS = "laborWindowUserDataOptions",
    LABOR_COST_CENTER_OPTS = "laborWindowCostCenterOptions",
    SAVED_SCHEDULES = "savedSchedules",
    LEAVE_TYPE_HOUR_OPTS = "leaveRequestWindowTypeHourOptions",
    LEAVE_CERTIFIER_OPTS = "leaveRequestWindowCertifierOptions",
    SAVED_LEAVE_REQS = "savedLeaveRequests",
    RDO = "rdoDate"
}